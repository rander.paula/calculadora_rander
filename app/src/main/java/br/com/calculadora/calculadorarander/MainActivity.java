package br.com.calculadora.calculadorarander;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button btnSoma, btnSub, btnMulti, btnDiv, btnLimpa;
    private EditText n1, n2;
    private TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Instaciando :
        //Botoes de operacoes :
        btnSoma = findViewById(R.id.btnSomar);
        btnSub = findViewById(R.id.btnSubtrair);
        btnMulti = findViewById(R.id.btnMultiplicar);
        btnDiv = findViewById(R.id.btnDividir);
        btnLimpa = findViewById(R.id.btnLimpar);

        //Espaços de valores a serem inseridos :
        n1 = findViewById(R.id.editTextN1);
        n2 = findViewById(R.id.editTextN2);

        //Saída da operação :
        resultado = findViewById(R.id.textViewResultado);


    }

    public void Somar(View view) {

        String A = n1.getText().toString().trim();
        String B = n2.getText().toString().trim();

        Double C = Double.parseDouble(A);
        Double D = Double.parseDouble(B);
        Double E = C + D;

        resultado.setText(E.toString());
    }

    public void Subtrair(View view) {

        String A = n1.getText().toString().trim();
        String B = n2.getText().toString().trim();

        Double C = Double.parseDouble(A);
        Double D = Double.parseDouble(B);
        Double E = C - D;

        resultado.setText(E.toString());
    }

    public void Multiplicar(View view) {

        String A = n1.getText().toString().trim();
        String B = n2.getText().toString().trim();

        Double C = Double.parseDouble(A);
        Double D = Double.parseDouble(B);
        Double E = (C * D);

        resultado.setText(E.toString());
    }

    public void Dividir(View view) {

        String A = n1.getText().toString().trim();
        String B = n2.getText().toString().trim();

        Double C = Double.parseDouble(A);
        Double D = Double.parseDouble(B);
        Double E = (C % D);

        resultado.setText(E.toString());
    }

    public void Limpar(View view){
        n1.setText(" ");
        n2.setText(" ");
        resultado.setText(" ");
    }

}
